### IRC Logistic (T-Systems Java School project)

* Task: [Google Docs](https://docs.google.com/document/d/1uiWsxqpKhPz0m6jQIP65pY4iL0W6hA3AeZ5pBjNqknI/edit?usp=sharing)
* Technical Solution Description: [Confluence](https://ruddi.atlassian.net/l/c/xpCBRB47)

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=ivan_rud_logiweb)](https://sonarcloud.io/dashboard?id=ivan_rud_logiweb)